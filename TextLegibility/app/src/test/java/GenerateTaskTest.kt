import org.junit.Assert.assertTrue
import org.junit.Test
import uniregensburg.textlegibility.util.GenerateTask

class GenerateTaskTest {


    @Test
    fun generateFindLetters() {
        val task = GenerateTask.generate()

        System.out.println(task)

        val findLetter = task.letters

        val firstLine = findLetter.split("\n")[0]
        val secondLine = findLetter.split("\n")[1]

        assertTrue(firstLine.length == 6)
        assertTrue(secondLine.length == 6)

        val test = firstLine + secondLine

        var counter = 0;
        val splittBy = arrayOf(0, 2, 4, 6, 8, 10)
        for (i in splittBy) {
            val splitted = test.substring(i, i + 2)
            if (splitted[0].equals(splitted[1].toUpperCase())) {
                counter++
            }
        }
        assertTrue(counter == 1)
    }


    @Test
    fun generateCountLetters() {
        val task = GenerateTask.generate()
        System.out.println(task)

        val countLetters = task.count

        val firstLine = countLetters.split("\n")[0]
        val secondLine = countLetters.split("\n")[1]

        assertTrue(firstLine.length == 6)
        assertTrue(secondLine.length == 6)

        val test = firstLine + secondLine

        var counter = 0;
        for (i in 0 until test.length) {
            if (test[i].equals(task.duplicate)) {
                counter++
            }
        }
        assertTrue(counter > 0 && counter < 4)
        assertTrue(counter == task.result)
    }

}