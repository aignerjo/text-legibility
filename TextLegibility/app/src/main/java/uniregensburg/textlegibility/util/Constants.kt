package uniregensburg.textlegibility.util

import android.support.annotation.StringDef

const val BASE_URL = "http://192.168.178.94:8080/";

const val SETTINGS = "settings"
const val TUTORIAL = false
const val LIVE = true

const val ITERATIONS = "iterations"
const val LIVE_ITERATIONS = 16
const val TUTORIAL_ITERATIONS = 6


const val USERID = "user_id"

const val PLAIN = "plain"
const val BILLBOARD = "billboard"

const val METHOD = "method"
const val STANDING = "standing"
const val WALKING = "walking"
const val ACTIVE = "active"
const val PASSIVE = "passive"
