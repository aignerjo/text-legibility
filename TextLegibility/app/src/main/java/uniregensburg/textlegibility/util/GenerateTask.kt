package uniregensburg.textlegibility.util

import uniregensburg.textlegibility.task.Task
import uniregensburg.textlegibility.task.TaskMethods
import java.util.*

class GenerateTask {

    companion object Factory {

        val random = Random()
        val restrictedAlphabet = arrayOf('C', 'K', 'M', 'O', 'P', 'S', 'U', 'V', 'W', 'X', 'Z')
        val restrictedPositions = arrayOf(0, 2, 4, 6, 8, 10)
        var duplicate: Char = 'a'
        var result: Int = -1


        fun generate(): Task {
            val letters = generateLetters()
            val count = generateCount()

            return Task(letters, count, result, duplicate, TaskMethods.WALKING_VS_SITTING)
        }


        private fun generateLetters(): String {

            var res = generateRandomLetters()
            res = generateOneDuplicate(res)
            res = upperLowerCase(res)
            res = splitInTwoLines(res)

            return res

        }

        private fun generateCount(): String {
            var res = generateRandomLetters()
            res = assureCountIsInRange(res)
            res = splitInTwoLines(res)
            return res
        }


        private fun assureCountIsInRange(letters: String): String {
            var resultLetters = letters;

            val wanted = random.nextInt(3) + 1;
            var current = getCurrentOccurence(resultLetters)

            result = wanted;


            if (wanted == current) {
                return resultLetters
            }


            while (current < wanted) {
                resultLetters = addDuplicateToRandomPos(resultLetters, randPosOverall());
                current = getCurrentOccurence(resultLetters)
            }

            while (current > wanted) {
                var otherChar = randChar()
                resultLetters = removeDuplicate(resultLetters, randChar())
                current = getCurrentOccurence(resultLetters)
            }

            return resultLetters
        }


        private fun removeDuplicate(letters: String, char: Char): String {
            var result = letters;
            return result.replaceFirst(duplicate, char)
        }

        private fun addDuplicateToRandomPos(letters: String, pos: Int): String {
            var result = ""
            for (i in 0 until letters.length) {
                if (i == pos) {
                    result += duplicate;
                } else {
                    result += letters[i]
                }
            }
            return result
        }

        private fun getCurrentOccurence(letters: String): Int {
            var count = 0;
            for (i in 0 until letters.length) {
                if (letters[i].equals(duplicate)) {
                    count++
                }
            }
            return count;
        }


        private fun splitInTwoLines(letters: String): String {
            var firstLine = letters.substring(0, 6)
            var secondLine = letters.substring(6, 12);

            return firstLine + "\n" + secondLine;

        }

        private fun generateOneDuplicate(letters: String): String {
            var res = ""
            val pos = randPosInLine()
            duplicate = letters[pos]

            for (i in 0..11) {
                if (i != pos + 1) {
                    res += letters[i]
                } else {
                    res += duplicate
                }
            }
            return res
        }

        private fun generateRandomLetters(): String {
            var res = ""
            while (res.length < 12) {
                val randomLetter = randChar()
                if (res.isEmpty()) {
                    res += randomLetter
                } else {
                    val lastLetter = res[res.length - 1]
                    if (!lastLetter.equals(randomLetter)) {
                        res += randomLetter
                    }
                }
            }

            return res;

        }

        private fun randChar(): Char {
            return restrictedAlphabet[random.nextInt(11)]
        }

        private fun randPosInLine(): Int {
            return restrictedPositions[random.nextInt(6)]

        }

        private fun upperLowerCase(upper: String): String {
            var upperLower = ""
            for (i in 0..11) {
                if (i % 2 == 0) {
                    upperLower += upper[i]
                } else {
                    upperLower += upper[i].toLowerCase()
                }
            }
            return upperLower
        }

        private fun randPosOverall(): Int {
            return random.nextInt(12);
        }

    }

}