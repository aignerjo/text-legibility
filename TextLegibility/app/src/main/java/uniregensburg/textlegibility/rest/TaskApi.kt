package uniregensburg.textlegibility.rest

import io.reactivex.Observable

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uniregensburg.textlegibility.util.BASE_URL
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*
import uniregensburg.textlegibility.task.TaskResult


interface TaskApi {

    @POST("api/taskResult")
    @FormUrlEncoded
    fun write(@Field("taskResult") writeThis: TaskResult): Observable<TaskResult>


    companion object Factory {
        fun create(): TaskApi {
            val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()

            return retrofit.create(TaskApi::class.java);
        }
    }
}


