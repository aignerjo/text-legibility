package uniregensburg.textlegibility

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_select_task.*
import uniregensburg.textlegibility.util.*
import java.util.*

class SelectionActivity : AppCompatActivity() {

    val context: Context = this;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_task)

        initButtons()
    }


    fun initButtons() {
        standing.setOnClickListener{
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(SETTINGS, LIVE)
            intent.putExtra(ITERATIONS, LIVE_ITERATIONS)
            intent.putExtra(METHOD, STANDING)
            startActivity(intent)
        }

        walking.setOnClickListener{
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(SETTINGS, LIVE)
            intent.putExtra(ITERATIONS, LIVE_ITERATIONS)
            intent.putExtra(METHOD, WALKING)
            startActivity(intent)
        }

        active.setOnClickListener{
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(SETTINGS, LIVE)
            intent.putExtra(ITERATIONS, LIVE_ITERATIONS)
            intent.putExtra(METHOD, ACTIVE)
            startActivity(intent)
        }

        passive.setOnClickListener{
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(SETTINGS, LIVE)
            intent.putExtra(ITERATIONS, LIVE_ITERATIONS)
            intent.putExtra(METHOD, PASSIVE)
            startActivity(intent)
        }
    }


}
