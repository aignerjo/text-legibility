package uniregensburg.textlegibility.task

data class Task(val letters: String, val count: String, val result: Int, val duplicate: Char, val name: Enum<TaskMethods>)
