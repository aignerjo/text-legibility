package uniregensburg.textlegibility.task

enum class TaskMethods{

    ACTIVE_VS_PASSIVE, WALKING_VS_SITTING
}
