package uniregensburg.textlegibility.task


data class TaskResult(val responseTime: Long, val userInput: Int, val trueInput: Int, val method: String, val textStyle: String) {
    override fun toString(): String =
            "{" +
                    "\"method\": \"" + method + '"' +
                    ", \"textStyle\": \"" + textStyle + '"' +
                    ", \"responseTime\": \"" + responseTime + '"' +
                    ", \"userInput\": \"" + userInput + '"' +
                    ", \"correct\": \"" + trueInput + '"' +
                    "}";
}
