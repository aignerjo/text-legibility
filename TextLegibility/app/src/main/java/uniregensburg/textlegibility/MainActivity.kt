package uniregensburg.textlegibility

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import uniregensburg.textlegibility.util.*
import java.util.*

class MainActivity : AppCompatActivity() {

    val context: Context = this;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT, AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT)
        getWindow().getDecorView().setSystemUiVisibility(3328)
        setContentView(R.layout.activity_main)

        initButtons()
    }


    fun initButtons() {
        examples.setOnClickListener {
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(SETTINGS, TUTORIAL)
            intent.putExtra(ITERATIONS, TUTORIAL_ITERATIONS)
            intent.putExtra(METHOD, "")
            startActivity(intent)
        }

        start.setOnClickListener {
            val intent = Intent(context, SelectionActivity::class.java)
            startActivity(intent)
        }
    }


}
