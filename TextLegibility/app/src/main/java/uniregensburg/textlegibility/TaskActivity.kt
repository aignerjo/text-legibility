package uniregensburg.textlegibility

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_task.*
import uniregensburg.textlegibility.rest.TaskApi
import uniregensburg.textlegibility.task.Task
import uniregensburg.textlegibility.task.TaskResult
import uniregensburg.textlegibility.util.*


class TaskActivity : AppCompatActivity() {

    var ready: Boolean = true
    var iterations: Int = 0
    var currentIteration: Int = 0

    val taskService = TaskApi.create();

    var currTime: Long = System.currentTimeMillis()
    var task: Task = GenerateTask.generate()

    var record: Boolean = false

    var method: String = ""
    var timeout: Long = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        getSettings()

        initTask()
    }

    fun getSettings() {
        record = intent.extras.getBoolean(SETTINGS)
        iterations = intent.extras.getInt(ITERATIONS)
        method = intent.extras.getString(METHOD)

        if (method.equals("")) {
            timeout = 4000;
        } else {
            timeout = 1000;
        }
    }

    override fun onResume() {
        super.onResume()
        initTask()
    }

    fun initTask() = if (currentIteration <= iterations) {
        task = GenerateTask.generate()
        displayTask()
        currTime = System.currentTimeMillis()
        currentIteration += 1
    } else {
        displaySuccess(true)
    }


    fun postUserInput(time: Long, userInput: Int, textStyle: String) {

        var taskResult: TaskResult = TaskResult(time, userInput, task.result, method, textStyle)

        var observable: Observable<TaskResult> = taskService.write(taskResult)

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { responseData ->
                    Log.d("TEST", responseData.toString())
                }
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN && dpadAction(event.keyCode) && ready) {
            val keyCode = event.keyCode
            val input = getInputFromUser(keyCode)
            showResult(input)
            ready = false
            val timeUsed = System.currentTimeMillis() - currTime
            val textStyle = if (currentIteration % 2 == 0) BILLBOARD else PLAIN;
            if (record) postUserInput(timeUsed, input, textStyle)
            startTimerForNewExample()
        }
        return super.dispatchKeyEvent(event)
    }

    private fun startTimerForNewExample() {
        Handler().postDelayed({
            initTask()
        }, timeout)
    }


    private fun displaySuccess(display: Boolean) {
        trueResult.visibility = if (display) View.INVISIBLE else View.VISIBLE
        userInput.visibility = if (display) View.INVISIBLE else View.VISIBLE
        upperBox.visibility = if (display) View.INVISIBLE else View.VISIBLE
        lowerBox.visibility = if (display) View.INVISIBLE else View.VISIBLE

        success.visibility = if (display) View.VISIBLE else View.INVISIBLE
        if (display) {
            Handler().postDelayed({
                success.visibility = View.INVISIBLE
                finish()
            }, 3000)
        }
    }

    private fun displayTask() {

        if (currentIteration % 2 == 0) {
            displayPlainText()
        } else {
            displayBillboard()
        }

        trueResult.visibility = View.INVISIBLE
        userInput.visibility = View.INVISIBLE

        upperBox.text = task.letters
        lowerBox.text = task.count

        trueResult.text = Integer.toString(task.result)


        ready = true;
    }


    private fun displayBillboard() {
        upperBox.setTextColor(Color.WHITE)
        lowerBox.setTextColor(Color.WHITE)

        upperBox.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
        lowerBox.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
    }

    private fun displayPlainText() {
        upperBox.setTextColor(ContextCompat.getColor(this, R.color.red))
        lowerBox.setTextColor(ContextCompat.getColor(this, R.color.red))

        upperBox.setBackgroundColor(Color.BLACK)
        lowerBox.setBackgroundColor(Color.BLACK)
    }

    private fun showResult(input: Int) {
        if (!record) {
            trueResult.visibility = View.VISIBLE
            userInput.text = Integer.toString(input)
            userInput.visibility = View.VISIBLE
        }
    }


    private fun getInputFromUser(keyCode: Int): Int {
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            return 1
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            return 2
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            return 3
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            return 0
        }
        return -1
    }

    private fun dpadAction(keyCode: Int): Boolean {
        return (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_DOWN)
    }


}

