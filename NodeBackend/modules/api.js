const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const tasks = require('../domain/task-result');
const port = 8080;


let listen = () => {
    app.listen(port, () => console.log(`Listening on port ${port}`));
};

app.post('/api/taskResult', (req, res) => {
    let task = JSON.parse(req.body.taskResult);
    let result = tasks.addResult(task);
    tasks.logResult(result);
    res.send(task)
});


module.exports = {
    listen
};