const fs = require('fs');

const tasks = require('../domain/task-result');

const path = "results.csv";

let exportJson = () => {
    let results = tasks.fetchResults();
    if (results.length > 0) {
        let csv = "";
        for (r of results) {
            let errorRate = tasks.calcErrorRate(r.userInput, r.correct);
            let line = `${r.userID}, ${r.method}, ${r.textStyle}, ${r.responseTime}, ${r.userInput}, ${r.correct}, ${errorRate}\n`;
            csv += line;
        }
        write(csv);
    }else{
        console.log("Nothing to write - JSON is currently empty!")
    }

};

let write = (csv) => {
    fs.writeFileSync(path, csv);
    console.log("Write to CSV completed - check results.csv");

};

module.exports = {
    exportJson
};