const _ = require('lodash');
const yargs = require('yargs');


const api = require('./modules/api');
const write = require('./modules/write');
const tasks = require('./domain/task-result');


const idOptions = {
    descripe: 'PersonID',
    demand: true,
    alias: 'i'
};

const argv = yargs
    .command('add', 'Add a new person', {
        id: idOptions
    })
    .command('write', 'Writes current version to .csv')
    .help()
    .argv;


let command = argv._[0];


if (command === 'add') {
    let idTaken = tasks.idTaken(argv.id);
    if(idTaken){
        console.log('ID is taken - please select another one.');
        process.exit();
    }
    tasks.setID(argv.id);
    api.listen();
}else if(command === 'write'){
    write.exportJson();
} else {
    console.log('No command recognized.')
}
