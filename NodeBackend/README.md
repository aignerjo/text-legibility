# Towards text-legibility in Augmented Reality

## Backend with node.js
Use this command to get help:
    
    node app.js --help 
    
Use this command to create a new person with an id: 

    node app.js add -i <id>
    
Use this command to write the results to a fromatted csv:

    node app.js write 
    
## Frontend with Kotlin
Currently there is a Tutorial Activity where users can try the application without writing anything to the backend. 

To start the Tasks for the users they must click the "Start" Button. 

The next Screen shows four selections for our four tasks:

* Walking ("Gehen")
* Standing ("Stehen")
* Active Background ("Aktiv")
* Passive Background ("Passiv")

Upon the selection the tasks begins **instantly**. 

### Current Setting

**Tutorial Iterations:** 6 Iterations (3 for each TextStyle)

**Live Iterations:** 10 Iterations (5 for each TextStyle) 

_Note:_ Those Iterations aren't random - The TextStyles change **alternately**.

To change these settings update the Constants.kt file.