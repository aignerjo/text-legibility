const fs = require('fs');

const path = '././results.json';

let id;

let setID = (id) => {
    this.id = id;
};

let getID = () => {
    return this.id;
};

let fetchResults = () => {
    try {
        let resultsString = fs.readFileSync(path);
        return JSON.parse(resultsString);
    } catch (e) {
        return [];
    }
};

let saveResults = (result) => {
    fs.writeFileSync(path, JSON.stringify(result));
};

let addResult = (result) => {
    result.userID = getID();
    let results = fetchResults();
    results.push(result);
    saveResults(results);
    return result;
};

let getAll = () => {
    return fetchResults();
};

let idTaken = (id) => {
    return fetchResults().filter((res) => res.userID === id).length > 0
};

let logResult = (result) => {
    console.log('--');
    console.log(`UserID: ${result.userID}`);
    console.log(`Method: ${result.method}`);
    console.log(`TextStyle: ${result.textStyle}`);
    console.log(`UserInput: ${result.userInput}`);
    console.log(`Correct: ${result.correct}`);
    console.log(`ResponseTime: ${result.responseTime}`);
};

let calcErrorRate = (userInput, correct) => {
    let p = parseInt(userInput);
    let c = parseInt(correct);

    if(p === 0){
        return 3;
    }else{
        return Math.abs(c - p);
    }
};

module.exports = {
    calcErrorRate,
    setID,
    getID,
    idTaken,
    saveResults,
    fetchResults,
    addResult,
    getAll,
    logResult
};
