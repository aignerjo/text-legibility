##### Settings
Font-Size: 1 inch from 2m distance (half from Gabbard et al.)

Upper-Box: Find the letter (eg. 'Vv')

Lower-Box: Count the occurence of the letter

Iterations for each task: currently 10 - should be 20 ?

Video for active: Neupfarrplatz

Screenshot for passive: Neupfarrplatz


##### QUESTIONS AFTER EACH TASK:
1. Did you have any problems?
2. Did you notice something?
3. Was it difficult for you? 


##### BEFORE TASK:
1. Mark the controller with 1, 2, 3 and 0 (take picture)
2. Prepare the Task-Setups:
	1. Course for walking (white wall -> white wall) (take picture)
	2. TV (with distance > 2m) (take picture)
	3. Screenshot from Video-Footage for passive

##### FOR SCRIPT:
1. Intel Smart-Glasses (red-text)
2. Font-Size: 1 inch from 2m distance (half from Gabbard et al.)

##### DEMOGRAFICAL: 
1. glasses vs. no glasses vs. contact lenses
2. age
3. sex

